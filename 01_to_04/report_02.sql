SELECT DISTINCT (c.last_name ||' '|| c.first_name) "Nom et Pr�nom"
FROM t_reservation r
JOIN t_ticket t
ON r.buyer_id != t.customer_id
JOIN t_customer c
ON r.buyer_id = c.customer_id
ORDER BY "Nom et Pr�nom";