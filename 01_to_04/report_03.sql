SELECT r.reservation_id "Num r�servation", r.creation_date "Date cr�ation", e.last_name ||' '|| e.first_name " Nom et Pr�nom employ� ", c.last_name||' '|| c.first_name " Nom et Pr�nom client "
FROM t_reservation r JOIN t_employee e
ON (r.employee_id = e.employee_id)
JOIN t_customer c
ON (c.customer_id = r.buyer_id)
WHERE r.creation_date = (SELECT Min(r.creation_date) 
			         FROM t_reservation r);