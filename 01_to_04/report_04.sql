SELECT c.last_name||' '|| c.first_name "Nom et Pr�nom",
CASE  
WHEN (Months_between('30/11/2016', c.pass_date)>12) THEN 'Perim� !'
WHEN (Months_between('30/11/2016', c.pass_date) is NULL) THEN 'Aucun'
ELSE p.pass_name
END "Type de pass"
FROM t_customer c
LEFT OUTER JOIN t_pass p
ON (p.pass_id = c.pass_id)
ORDER BY "Nom et Pr�nom" ;