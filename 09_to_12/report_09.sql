SELECT train_id||' '||d.city||' - '||a.city "Nom Train", 
ROUND((distance/((arrival_time-departure_time)*1440))*60, 0)||' Km/H' "Vitesse Train"
FROM t_train t
JOIN t_station d
ON t.departure_station_id = d.station_id
JOIN t_station a
ON t.arrival_station_id = a.station_id ;