SELECT COUNT(c.customer_id) "Reduc disp S�niors 01/17"
FROM t_customer c 
JOIN t_pass p ON c.pass_id = p.pass_id
JOIN t_ticket t ON c.customer_id = t.customer_id
JOIN t_reservation r ON t.reservation_id = r.reservation_id
JOIN t_wagon_train w ON t.wag_tr_id = w.wag_tr_id
JOIN t_train tt ON w.train_id = tt.train_id
WHERE p.pass_name = 'Senior'
AND (r.creation_date-c.pass_date)<365
AND r.price IS NOT NULL
AND tt.departure_time LIKE '%/01/17';