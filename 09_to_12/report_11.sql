SELECT t.train_id||' '||s1.city||' - '||s2.city "Nom Train", p.pass_name "Titre Abonnement", t.price-((t.price*p.discount_pct)/100) "Tarif Semaine en euros", t.price-((t.price*p.discount_we_pct)/100) "Tarif Week-end en euros"
FROM t_train t
CROSS JOIN t_pass p
JOIN t_station s1 ON t.departure_station_id = s1.station_id
JOIN t_station s2 ON t.arrival_station_id = s2.station_id
WHERE s1.city = 'Paris'
ORDER BY t.train_id ;