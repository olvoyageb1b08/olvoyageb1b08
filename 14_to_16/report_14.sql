SELECT tt.train_id||' '||s1.city||' - '||s2.city "Nom Train", 
    SUM(nb_seat)-COUNT(t.ticket_id) "Nombre de places libres"
FROM t_train tt
JOIN t_wagon_train wt ON tt.train_id = wt.train_id
JOIN t_wagon w ON w.wagon_id = wt.wagon_id
JOIN t_station s1 ON tt.departure_station_id = s1.station_id
JOIN t_station s2 ON tt.arrival_station_id = s2.station_id
JOIN t_ticket t ON t.wag_tr_id = wt.wag_tr_id
JOIN t_reservation r ON t.reservation_id = r.reservation_id
WHERE tt.distance > 300
AND tt.departure_time LIKE '22/01/17'
GROUP BY tt.train_id||' '||s1.city||' - '||s2.city
HAVING COUNT(t.ticket_id) > 0
ORDER BY tt.train_id||' '||s1.city||' - '||s2.city ;